// Import the Amazon S3 service client
var S3 = require("aws-sdk/clients/s3");
// import csv parser and writer
const csv = require("fast-csv");
// Set credentials and Region
var s3 = new S3({
  apiVersion: "2006-03-01",
  region: "us-west-1",
  credentials: {
    accessKeyId: "HERE YOUR ACCESS ID",
    secretAccessKey: "HERE YOUR SECRET",
  },
});
const bucketInfo = {
  Bucket: "BUCKET NAME", // The s3 bucket name we use
};

async function listAllKeys() {
  const { Contents: rawFilesData } = await s3.listObjects(bucketInfo).promise();
  return rawFilesData.map((o) => o.Key);
}

const parseCSV = (fileName) => {
  const params = { ...bucketInfo, Key: fileName };
  const s3Stream = s3
    .getObject(params)
    .createReadStream()
    .pipe(csv.parse({ headers: true }))
    .on("error", (error) => console.error(error))
    .on("data", (row) => {
      console.log(row);
      // do something with each row
      // like inserting them in the DB
    })
    .on("end", (rowCount) => console.log(`Parsed ${rowCount} rows`));
};

const init = async () => {
  const keys = await listAllKeys(bucketInfo.Bucket); // Keys is now an array of all filenames

  keys.forEach((key) => {
    console.log("Key is : ", key);
    // parse each CSV file
    parseCSV(key);
  });
};

init();
